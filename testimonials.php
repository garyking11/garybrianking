<?php include 'includes/head.php'; ?>

<body class="<?php echo $page_title; ?>">

    <div id="wrapper"  >

        <div class="top_wrapper">

            <?php include 'includes/header.php'; ?><!-- nav -->


            <!-- "It was a pleasure working with Gary. He is conscientious, has a solid work ethic and a can do attitude." ~ Purdie Rogers
            
            "Gary was great from the minute I called him!" ~Tera Vintage
            
            "The atmosphere in the sales department at KOMO TV4 is fast paced, chaotic, high-pressure … KOMOTV4
            
            -->
            <div class="top-title-wrapper">
                <div class="container">
                    <div class="row">

                        <div class="col-md-12 col-sm-12 page-info">
                            <h1 class="h1-page-title"><?php echo $page_title; ?></h1> 
                        </div>
                    </div>
                </div>
            </div>
        </div><!--.top wrapper end -->

        <div class="loading-container">
        
            <div class="loading">
                <i></i><i></i>
            </div>
            <div class="loading-fallback">
                <img src="images/loading.gif" alt="Loading"/>
            </div>
            <div class="loading-text">
                loading..
            </div>
        </div>

        <!-- start testimonials -->

        <div class="container">
            <div class="space-sep40"></div>
            <div class="row">



                <div class="col-md-8 col-sm-8">

                   <!-- start testimonial -->
                    <div class="testimonial-vertical">

                        <div class="testimonial-vertical-pics">
                            <ul>
                                <li>
                                    <a href=" ">

                                        <img src="images/avatars/jordan-b.jpg" alt="Jordan Brown"/>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <!-- Arrow -->
                        <div class="testimonial-vertical-arrow"></div>
                        <!-- Arrow -->

                        <div class="testimonial-vertical-content">                    
                            <div class="testimonial-vertical-text" id="accord-1">

                                <div>
                                     "Gary was our go-to developer for anything tricky. He has a passion for solving complex questions with usable solutions. Gary is always looking for that better/faster/prettier way to accomplish the desired result. His creative and fun personality, paired with his incredible technical abilities, make him a vital asset to any team. "
                                    <br /><br />I highly recommend Gary.  
                                    

                                </div>

                                <div class="testimonial-vertical-author">
                                    Jordan Brown
                                    <div class="testimonial-vertical-author-position gray-text">
                                    Production Designer at Redfin
                                </div>
                                     <span class="gray-text-italic">Jordan worked directly with Gary at Sesame Communications as a project manager.</span>
                                </div>



                            </div><!-- end testimonial-vertical-text -->  
                        </div><!-- testimonial-vertical-content -->
                        <div class="space-sep20"></div>

                        <!-- end testimonial -->
                        
                        
                        
                        
                        
                        
                        
                    <!-- start testimonial -->
                    <div class="testimonial-vertical">

                        <div class="testimonial-vertical-pics">
                            <ul>
                                <li>
                                    <a href=" ">

                                        <img src="images/avatars/jon-k.jpg" alt="Jon Kaufman"/>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <!-- Arrow -->
                        <div class="testimonial-vertical-arrow"></div>
                        <!-- Arrow -->

                        <div class="testimonial-vertical-content">                    
                            <div class="testimonial-vertical-text" id="accord-1">

                                <div>
                                     As Gary's manager, I can describe him as conscientious, innovative, knowledgeable with a solid work ethic. He is a seasoned web developer with advanced skills in html5, php, jQuery, php frameworks, CMS, and Adobe Creative Suite. 

                                    <br /><br />I would highly recommend Gary. He would be a great asset to any organization.  
                                    

                                </div>

                                <div class="testimonial-vertical-author">
                                    Jon Kauffman
                                    <div class="testimonial-vertical-author-position gray-text">
                                    Project Manager at Plexipixel
                                </div>
                                     <span class="gray-text-italic">Jon worked directly with Gary at Sesame Communications as his manager</span>
                                </div>



                            </div><!-- end testimonial-vertical-text -->  
                        </div><!-- testimonial-vertical-content -->
                        <div class="space-sep20"></div>

                        <!-- end testimonial -->


                        <!-- start testimonial-->          
                        <div class="testimonial-vertical">

                            <div class="testimonial-vertical-pics">
                                <ul>
                                    <li>
                                        <a href="#accord-1">
                                            <img src="images/avatars/matt-j.jpg" alt="Matt J"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <!-- Arrow -->
                            <div class="testimonial-vertical-arrow"></div>
                            <!-- Arrow -->

                            <div class="testimonial-vertical-content">

                                <div class="testimonial-vertical-text" id="accord-1">

                                    <div>


                                        Gary has a passion for web development spending many years of self-study to improve his programming skills. One of his most impressive projects is working extensively on the framework of Sesame Communications's customers' websites to cut down production time.<br /><br />

                                        He speaks his mind at meetings with other teams with solutions to remove roadblocks for all parties.<br /><br />

                                       
                                    </div>

                                    <div class="testimonial-vertical-author">
                                        Matt Jennings
                                        <br /><div class="testimonial-vertical-author-position gray-text">
                                        Production Artist at Sesame Communications
                                    </div>
                                         
                                         <span class="gray-text-italic">Matt worked directly with Gary at Sesame Communications</span>
                                    </div>



                                </div>
                            </div>    
                        </div>            

                        <div class="space-sep20"></div>

                        <!-- end testimonial -->

                        <!-- start testimonial -->
                        <div class="testimonial-vertical">

                            <div class="testimonial-vertical-pics">
                                <ul>
                                    <li>
                                        <a href="#accord-1">
                                            <img src="images/avatars/brittany-h.jpg" alt="Brittany H"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <!-- Arrow -->
                            <div class="testimonial-vertical-arrow"></div>
                            <!-- Arrow -->

                            <div class="testimonial-vertical-content">

                                <div class="testimonial-vertical-text" id="accord-1">

                                    <div>

                                        Gary is one of my all-time favorite web developers. His work is clean and beautiful. He is able to tackle crazy requests that we've never done before (mobile features, responsive template designs, in-house CMS, automated features, etc) and he keeps a pleasant demeanor and clear communication with the rest of the team. 

                                        <br /><br />I highly recommend Gary for front end development, and hope to work with him again someday!
                                       

                                    </div>

                                    <div class="testimonial-vertical-author">
                                        Brittany Hunt
                                        <div class="testimonial-vertical-author-position gray-text">
                                        Project Manager at Dynamo Plus
                                    </div>
                                         <span class="gray-text-italic">Brittany worked directly with Gary at Sesame Communications</span>
                                    </div>



                                </div>
                            </div>  
                        </div>              

                        <div class="space-sep20"></div>

                        <!-- end testimonial -->   

                        <!-- start testimonial -->
                        <div class="testimonial-vertical">

                            <div class="testimonial-vertical-pics">
                                <ul>
                                    <li>
                                        <a href="#accord-1">
                                             <img src="images/avatars/placeholder.png" alt="Geo Purdie"/>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <!-- Arrow -->
                            <div class="testimonial-vertical-arrow"></div>
                            <!-- Arrow -->

                            <div class="testimonial-vertical-content">

                                <div class="testimonial-vertical-text" id="accord-1">

                                    <div>

                                         It was a pleasure working with Gary. He is conscientious, has a solid work ethic and a can do attitude.  

                                    </div>

                                    <div class="testimonial-vertical-author">
                                        Geo Purdie  
                                    </div>

                                    <div class="testimonial-vertical-author-position gray-text">
                                        Owner at Purdie Rogers Advertising
                                    </div>

                                </div>
                            </div>                

                            <div class="space-sep20"></div>

                            <!-- end testimonial -->                                 


                           <!-- start testimonial -->
                            <div class="testimonial-vertical">

                                <div class="testimonial-vertical-pics">
                                    <ul>
                                        <li>
                                            <a href="#accord-1">
                                                <img src="images/avatars/renee-l.jpg" alt="Renee Lico"/>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <!-- Arrow -->
                                <div class="testimonial-vertical-arrow"></div>
                                <!-- Arrow -->

                                <div class="testimonial-vertical-content">

                                    <div class="testimonial-vertical-text" id="accord-1">

                                        <div>

                                             I have worked with Gary for a couple of years at Sesame Communications. Gary was often the one that created the sites that I was managing. <br /><br />I could always count on him for solid builds and the willingness to try new things. He has been an important asset in helping us streamline processes and efficiencies of the web team. Gary is great to work with and a valuable asset to any team. 
                                           

                                        </div>

                                        <div class="testimonial-vertical-author">
                                            Renee Lico
                                            
                                        <div class="testimonial-vertical-author-position gray-text">
                                            Web Development Producer at Sesame Communications
                                        </div>                                            
                                        <span class="gray-text-italic">Renee worked directly with Gary at Sesame Communications</span>
                                        </div>



                                    </div>
                                </div>                

                                <div class="space-sep20"></div>

                                <!-- end testimonial -->                    

                           <!-- start testimonial -->
                            <div class="testimonial-vertical">

                                <div class="testimonial-vertical-pics">
                                    <ul>
                                        <li>
                                            <a href="#accord-1">
                                                <img src="images/avatars/judy-j.jpg" alt="Judy Johnson"/>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <!-- Arrow -->
                                <div class="testimonial-vertical-arrow"></div>
                                <!-- Arrow -->

                                <div class="testimonial-vertical-content">

                                    <div class="testimonial-vertical-text" id="accord-1">

                                        <div>

                                             Gary is a capable and creative web developer who is always available to help other team members. 
                                             <br />Gary goes above and beyond to find new development solutions and would be an asset to any development company.

                                        </div>

                                        <div class="testimonial-vertical-author">
                                            Judy Johnson
                                            
                                        <div class="testimonial-vertical-author-position gray-text">
                                            Web Developer at Sesame Communications
                                        </div>                                            
                                        <span class="gray-text-italic">Judy worked directly with Gary at Sesame Communications</span>
                                        </div>



                                    </div>
                                </div>                

                                <div class="space-sep20"></div>

                                <!-- end testimonial -->        

                                <!-- start testimonial -->
                                <div class="testimonial-vertical">

                                    <div class="testimonial-vertical-pics">
                                        <ul>
                                            <li>
                                                <a href="#accord-1">
                                                     <img src="images/avatars/placeholder.png" alt="Geo Purdie"/>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                    <!-- Arrow -->
                                    <div class="testimonial-vertical-arrow"></div>
                                    <!-- Arrow -->

                                    <div class="testimonial-vertical-content">

                                        <div class="testimonial-vertical-text" id="accord-1">

                                            <div>

                                                 Gary was great from the minute I called him! 

                                            </div>

                                            <div class="testimonial-vertical-author">
                                                Teri Krantz 
                                            </div>

                                            <div class="testimonial-vertical-author-position gray-text">
                                                Owner at Tera Vintage
                                            </div>

                                        </div>
                                    </div>                

                                </div>        

                                <div class="space-sep20"></div>


                            </div></div>
                    </div></div></div>
        </div>
    </div><!--.content-wrapper end -->

    <?php include 'includes/footer.php'; ?>