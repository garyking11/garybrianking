<?php include 'includes/head.php'; ?>

<body  class="<?php echo $page_title; ?>">
 
<div id="wrapper"  >

<div class="top_wrapper">

<?php include 'includes/header.php'; ?><!-- nav -->
   <?php
/*print_r( $_REQUEST);
//exit;
//if (!isset($_POST['submit'])) {
 
if(!isset($_POST["submit"])) {
   // header("Location: contact.php");
		echo "no post";
    exit;
}

else {
	if(!$_POST) {
    header("Location: index.php");
    exit;
} else {
    $first_name = (isset($_POST['name'])) ? $_POST['name'] : NULL;
    $last_name = (isset($_POST['email'])) ? $_POST['email'] : NULL;
		
}


}
	$to = 'gary@garybking.com';  

	$subject = 'Web Inquiry';
	
	$message = 
		"Name: ". $_REQUEST["name"] . "\n" 
	. "Phone: " . $_REQUEST["phone"] . "\n" 
	. "Email: " . $_REQUEST["email"] . "\n\n" 
	. "Message: " .  wordwrap($_REQUEST["message"], 70);
	
	$headers = "From: ". $_REQUEST["name"] . $_REQUEST["email"]. "\r\n" 
	. "Reply-To: " . $_REQUEST["email"] . "\r\n" 
	. "Return-path: " . $_REQUEST["email"];
	
	 // mail($to,$subject,$message,"From: $from\n"  );
	
		
		//send the message, check for errors
				if ( ! mail($to,$subject,$message,$headers  )) {
						print_r(error_get_last());
						} else {
						
						echo "<p class='response-text'>Thanks " . $first_name .  ", for your message</p>";
						echo "<p class='response-text'>I will get back to you soon.</p>"; 
				}


//			}
	//		else
	//		{

//				echo 'There has been an error!';
//				echo '<a onclick="history.go(-1);"> Go Back</a>';
//				}

*/
?>


<div class="top-title-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 page-info">
                <h1 class="h1-page-title"><?php echo $page_title; ?></h1>

                
                </div> 
        </div>
    </div>
</div>
</div><!--.top wrapper end --> 

<div class="loading-container">
    <div class="loading">
        <i></i><i></i>
    </div>
    <div class="loading-fallback">
         <img src="images/loading.gif" alt="Loading"/>
    </div>
    <div class="loading-text">
        loading..
    </div>
</div>

<!-- Contact Map -->
<div class="body-wrapper">
    <div class="container">
        <div class="row">
        
          <div class="space-sep40"></div>    
          
            <div class="col-md-9 col-sm-9">
            
            <div id="contact-print">
             
                <form class="form-wrapper" id="contact-form" method="post" role="form"  ><!--<?php echo $_SERVER['PHP_SELF']; ?> novalidate>-->

                    <div class="form-group clearfix">
                        <label class="control-label" for="name">Name *</label>
                        <div class="col-xs-6">
                            <input type="text" id="name" name="name" required/><!-- class="form-control"  --> 
                        </div>
                    </div>

                    <div class="form-group clearfix">
                        <label class="control-label" for="email"> E-mail *</label>
                        <div class="col-xs-6">
                            <!-- type email used by jquery validate -->
                            <input type="text" name="email" id="email"  required/><!-- class="form-control"  -->
                        </div>
                    </div>
                    
                    <div class="form-group clearfix">
                        <label class="control-label" for="phone"> Phone</label>
                        <div class="col-xs-6">
                            <!-- type phone used by jquery validate -->
                            <input type="text" name="phone" id="phone"  /><!-- class="form-control"  -->
                        </div>
                    </div>                    

                    <div class="form-group clearfix">
                        <label class="control-label" for="message">  Message *</label>
                        <div class="col-xs-6">
                            <!-- type email used by jquery validate -->
                            <textarea name="message" id="message" required></textarea><!-- class="form-control"  --> 
                        </div>
                    </div>


                    <div class="form-group clearfix">
                        <label class="control-label"></label>
                        <div class="col-xs-6">
                            <input id="contact-button" type="button" value="Send" class="btn btn-primary btn-full-width" />
                        </div>
                    </div>
                </form>
                
                </div><!-- end contact-print -->
                
            </div>
            <div class="col-md-3 col-sm-3">



                <div class="sidebar-block">
                    <h3 class="h3-sidebar-title">
                        Contact
                    </h3>
                    <div class="sidebar-icon-item">
                        <i class="icon-phone"></i>  206-447-5196
                    </div>
                    <div class="sidebar-icon-item">
                        <i class="icon-envelope-alt"></i> 
                        <a class="email" rel="gary|garykingweb.com?subject=Web Inquiry" 
                                    title="Click here to email me">&nbsp;</a>
                    </div>
                   
                   
                
                </div>

                 
                <div class="sidebar-block">
                    
                    

<div class="social-icons">

    <ul>
                    
                  
                    <li>
                <a href="http://www.linkedin.com/in/garybking" title="linkedin" target="_blank" class="social-media-icon linkedin-icon">linkedin</a>
            </li>
                     
        
    </ul>

</div>
                </div>

            </div>
        </div>
    </div>
</div>
</div><!--.content-wrapper end -->

<?php include 'includes/footer.php'; ?>