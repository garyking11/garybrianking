<?php include 'includes/head.php'; ?>

<body class="<?php echo $page_title; ?>">
 
<div id="wrapper"  > 

<div class="top_wrapper">

<?php include 'includes/header.php'; ?><!-- nav -->

<div class="top-title-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 page-info">
                <h1 class="h1-page-title"><?php echo $page_title; ?></h1>
  					</div> 
        </div>
             
    </div>
</div>
</div><!--.top wrapper end -->

<div class="loading-container">
    <div class="loading">
        <i></i><i></i>
    </div>
    <div class="loading-fallback">
         <img src="images/loading.gif" alt="Loading"/>
    </div>
    <div class="loading-text">
        loading..
    </div>
</div>

<div class="space-sep40"></div>
    <div class="container">

                <!-- start example -->
 <div class="row">
            
            <div class="col-md-6 col-sm-6  right-text"  style="padding-left: 40px;">
            <h3 class="right-text">Sesame Communications Olive Template</h3>
                <p class="right-text"><a href="http://olive.samsesame.com/">View Live Site</a> &ndash; Template creation/management</p>
                <p class="right-text">HTML5, jQuery, Javascript, CSS3, PHP, AngularJS</p>
                
                <h4 style="text-align: right;"><br /><i class="icon-circle-arrow-right"></i>&nbsp;<a href="http://sesamewebdesign.com/sds-gallery.php">View more template creation/management examples</a></h4>
            

            </div>
            <div class="col-md-6 col-sm-6 left-text">
                <p>
                    <img src="media/examples/sesame-olive.jpg" alt="Sesame Olive Template" />       
                </p>                    
            </div>
        </div>              
                
                <!-- end example -->
        <hr /> 
        
        
       <!-- start example -->
 <div class="row">
            
            <div class="col-md-6 col-sm-6  right-text " >
            
                <p class="right-text">
                    <img src="media/examples/alta-air-logistics.jpg" alt="Alta Air Logistics" />       
                </p>
            </div>
            <div class="col-md-6 col-sm-6 left-text" style="padding-left: 40px;">
            <h3>Alta Air Logistics</h3>
                <p><!--<a href="http://www.shipalta.com/">-->Live Site Unavailable &ndash; <!--</a>--> &ndash; Classic Web Development</p>
                <p>HTML5, jQuery, Javascript, CSS3, Alta Air Logistics tracking API</p>
                     
            </div>
        </div>                
                <!-- end example -->
        <hr />             
                   
        <!-- start example -->
 <div class="row">
            
            <div class="col-md-6 col-sm-6  right-text"  style="padding-left: 40px;">
            <h3 class="right-text">Sesame Communications Madison Template</h3>
                <p class="right-text"><a href="http://madison.samsesame.com/">View Live Site</a> &ndash; Template creation/management</p>
                <p class="right-text">HTML5, jQuery, Javascript, CSS3, AngularJS</p>
            

            </div>
            <div class="col-md-6 col-sm-6 left-text">
                <p>
                    <img src="media/examples/sesame-madison.jpg" alt="Sesame Madison Template" />       
                </p>                    
            </div>
        </div>              
                
                <!-- end example -->
        <hr />        
     <!-- start example -->
 <div class="row">
            
            <div class="col-md-6 col-sm-6" >
                <p class="right-text">
                    <img src="media/examples/sage-surfaces.jpg" alt="Sage Surfaces" />       
                </p>
            </div>
            <div class="col-md-6 col-sm-6 left-text" style="padding-left: 40px;">
            <h3>Sage Surfaces</h3>
                <p> Live Site Unavailable &ndash; <strong>jQuery Color/Style selection UI/UX</strong></p>
                
                    <p>XHTML, jQuery, Javascript, CSS3, Google Mapping API</p>
            </div>
        </div>              
                
                <!-- end example -->    
<hr />                              
    <!-- start example -->
 <div class="row">
            
            <div class="col-md-6 col-sm-6  right-text"  style="padding-left: 40px;">
             <h3 class="right-text">Maplewood Dental</h3>
                <p class="right-text"><a href="http://www.maplewooddentalsmiles.com/">View Live Site</a> &ndash;  <strong>Responsive Web Development</strong></p>
                   <p class="right-text">HTML5, jQuery, Javascript, CSS3</p>            
            </div>
            <div class="col-md-6 col-sm-6  left-text">
                <p>
                    <img src="media/examples/maplewood-dental.jpg" alt="Maplewood Dental" />       
                </p>

            </div>
        </div>              
                
    <!-- end example -->
     <hr />           
    <!-- start example -->
    
 <div class="row">
            
            <div class="col-md-6 col-sm-6  right-text" >
                <p class="right-text">
                    <img src="media/examples/swelstad-orthodontics.jpg" alt="Swelstad Orthodontics" />       
                </p>
            </div>
            <div class="col-md-6 col-sm-6 left-text" style="padding-left: 40px;">
            <h3>Swelstad Orthodontics</h3>
                <p><a href="http://www.smilesbyswelstad.com/">View Live Site</a> &ndash; <strong>Classic Web Development</strong></p>
                   <p>HTML5, jQuery, Javascript, CSS3</p>
            </div>
        </div>              
                
                <!-- end example -->      
                
<hr />
                
    <!-- start example -->
 <div class="row">
            
            <div class="col-md-6 col-sm-6  right-text" style="padding-left: 40px;" >
<h3 class="right-text">Smileworx</h3>
            <p class="right-text"><a href="http://www.smilesbyswelstad.com/">View Live Site</a> &ndash; <strong>Classic Web Development</strong></p>
                
                    <p class="right-text">HTML5, jQuery, Javascript, CSS3</p>            
             </div>
            <div class="col-md-6 col-sm-6 left-text">
               <p>
                    <img src="media/examples/smileworx.jpg" alt="Smileworx" />       
                </p>
            
            </div>
        </div>              
                
                <!-- end example -->                                                                            
        
            <div class="space-sep40"></div>
            

 </div><!-- end container -->
</div>
</div><!--.content-wrapper end -->

<?php include 'includes/footer.php'; ?>