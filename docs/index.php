<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php 
$user=$_GET['user'];
//include "../include/dp-application-top-L1.php";
$path = $_SERVER['PHP_SELF'];
$page = basename($path, '.php');
$pageFile = basename($path, '');

?>
<title>Gary King Resume</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="content-language" content="en" />
<link type="text/css" rel="stylesheet" href="../css/style.css"  />
<meta name="robots" content="<?php //echo NOROBOTS;?>" />
<meta name="google-site-verification" content="ClbqZDm0fK3u8GsfgmgvOKLH2LpIDiLrKnOabUVC9Ts" /><!-- google -->
<?php //cssClient(); ?>

<link type="text/css" rel="stylesheet" href="css/css.css" />

</head><body>
<div id="header"><?php //headerImageClient(); ?></div>

<div id="wrapper">  
    <div class="page">	    
    <!-- content starts here -->    
        <div id="resume">

              <div class="leftContainerHomePage">	

        <h1>Resume</h1>
              <ul>
              <li class="bullet"><a href="lor.php" target="_self" title="Resume">View Resume</a></li>
              <li class="bullet"><a href="../../index.php" target="_self" title="Resume">View Website</a></li>
              <li class="bullet"><a href="http://www.garykingweb.com/dp-portfolio.php" target="_self" >View website examples</a></li>
              </ul>
              
              <div class="space20">&nbsp;</div>
              </div><!-- end leftContainerHomePage -->
               <div class="rightContainerHomePage">	

        	  <fieldset class="subset"><legend class="subset">Downloads</legend>              
              <ul>
              <h4>Letters of Reference</h4>
              <li class="pdf"><img src="../imgs/common/pdf.png" />&nbsp;&nbsp;<a href="letters-of-recommendation.pdf" target="_blank" title="Letters of Recommendation">KOMOTV4, Video Training, Inc</a></li>
              <li class="pdf"><img src="../imgs/common/pdf.png" />&nbsp;&nbsp;<a href="purdie-rogers-referral-letter.pdf" target="_blank" title="Purdie Rogers Referral">Purdie Rogers</a></li>
              </ul>
              
              <h4>Resume - Word Document</h4>
              
              <ul>
              <li class="pdf"><img src="../imgs/common/doc.png" />&nbsp;&nbsp;<a href="Gary-King-Resume.doc" target="_blank" title="Gary King Resume">Gary King Resume</a>
              </li>
              </ul>
              
              </fieldset>
              </div><!-- emd rightContainerHomePage -->
              <div class="clear">&nbsp;</div>
              
        </div>
        </div>
        

        <div class="space20">&nbsp;</div>
        

    </div>
    
    <script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-9485654-3");
pageTracker._trackPageview();
} catch(err) {}</script>
<div class="bottomNav"><?php //include 'include/bottom-nav.php'; ?></div>
<div class="footer"><?php echo COPYRIGHT; ?></div>



</body>
</html>
