<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php 
$user=$_GET['user'];
//include "../include/dp-application-top-L1.php";
$path = $_SERVER['PHP_SELF'];
$page = basename($path, '.php');
$pageFile = basename($path, '');

?>
<title>Gary King Resume</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="content-language" content="en" />

<meta name="robots" content="<?php echo NOROBOTS;?>" />
<meta name="google-site-verification" content="6-LQt1pXkLf4r7aRWnUx_fpsV3_r-BmecPHNCBJiaaA" /><!-- google -->
<?php //cssClient(); ?>
<link type="text/css" rel="stylesheet" href="css/css.css" />



</head><body>
<div id="header"><?php //headerImageClient(); ?></div>

<div id="wrapper">  
    <div class="page">	    
    <!-- content starts here -->    
        <div id="resume">
        	<div id="homePage">
             <div class="leftContainer">
        <h1>Resume</h1>

              <ul>
              <li class="bullet"><a href="index.php" target="_self" title="Resume">Resume Home Page</a></li>
              <li class="bullet"><a href="../../index.php" target="_self" title="Resume">View Website</a></li>
              <li class="bullet"><a href="http://www.garykingweb.com/dp-portfolio.php" target="_self" >View website examples</a></li>
              </ul>
              
              
              <div class="space20">&nbsp;</div>
              </div><!-- end leftColumnSeo -->
              <div class="rightContainer">	
               
        	         	  <fieldset class="subset"><legend class="subset">Downloads</legend>              
              <ul>
              <h4>Letters of Reference</h4>
              <li class="pdf"><img src="../imgs/common/pdf.png" />&nbsp;&nbsp;<a href="letters-of-recommendation.pdf" target="_blank" title="Letters of Recommendation">KOMOTV4, Video Training, Inc</a></li>
              <li class="pdf"><img src="../imgs/common/pdf.png" />&nbsp;&nbsp;<a href="purdie-rogers-referral-letter.pdf" target="_blank" title="Purdie Rogers Referral">Purdie Rogers</a></li>
              </ul>
              
              <h4>Resume - Word Document</h4>
              
              <ul>
              <li class="pdf"><img src="../imgs/common/doc.png" />&nbsp;&nbsp;<a href="Gary-King-Resume.doc" target="_blank" title="Gary King Resume">Gary King Resume</a>
              </li>
              </ul>
              
              </fieldset>
              
              </div><!-- emd roghtcolumnSeo -->
              <div class="clear">&nbsp;</div>
              
       		</div>
        </div>
       
        <div class="pagePanelBorder">












<h4>07/01/13</h4>

<h2>Gary King</h2>


<p>206-447-5196<br/>
gary@garykingweb.com</p>

<h3>Title: Web Developer</h3>

<p>I am interested in full time employment.  I have listed my experience in the following pages.</p>

<p>Please visit my web site http://www.garykingwebdevelopment.com for an overview, online resume and examples of my work.</p>

<h4>Letters of recommendation can be found at these links. </h4>
<p>http://www.garykingwebdevelopment.com/resume/letters-of-recommendation.pdf
http://www.garykingwebdevelopment.com/resume/purdie-rogers-referral-letter.pdf </p>

<p>Thanks, </p>



<p>Gary King<br />
www.garykingwebdevelopment.com/<br />
gary@garykingweb.com<br />
http://garykingwebdevelopment.com/site/resume
</p>











<h2>R e l e v a n t   W o r k</h2>

<h3>Sesame Communications, Inc January 2011 to present. – Frontend Web Developer</h3>

<ul>
<li>Building websites on a high energy, fast paced web production team. (2.5 years).</li>
<li>Currently I build the premium, custom web sites for orthodontic/dental industry at Sesame Communications.  I work with world class designers building high end custom sites.</li>
<li>Created the "responsive" templates for Sesame Communications' web sites.</li>
<li>300+ sites built  with an emphasis on "pixel perfect QA."</li>
<li>I also have  the responsibility of maintaining library content for these sites in our Designer Resources library.</li>
<li>I train new hires.</li>
<li>I have created the new web templates for the non-custom sites at Sesame Communications. (SDS)</li>
<li>I have created the web templates for  the "Landing Pages" product at Sesame.</li>
<li>Experienced with working on a team.</li>
</ul> 

<h4>A FEW EXAMPLES</h4>
<ul>
<li>http://www.summitpediatricdentistry.com/</li>
<li>http://www.kohrsortho.com/</li>
<li>http://www.daviesortho.com/</li>
<li>http://www.lifesmilesdds.com/</li>
<li>http://www.ossiorthodontics.com/</li>
<li>http://www.orthodonticsnewmarket.com/meet-dr-barry-shapero</li>
<li>http://www.silbermandental.com/</li>
<li>http://www.aventurapediatricdentistry.com/</li>
<li>http://www.carmicaldds.com/</li>
<li>http://www.drnikolovski.com/</li>
<li>http://www.smilesbyswelstad.com/</li>
<li>http://www.drlupiortho.com/</li>
<li>http://www.gireorthodontics.com/</li>
<li>http://www.scottortho.com/</li>
<li>http://www.fehrortho.com/</li></li>
<li>http://donn-craig.sesamehub.com/
<li>http://www.drsnow.com/</li>




<li>Technology and IDE's used: XHTML, HTML5, AJAX, jQuery, CSS, PHP, Dreamweaver CS5, Netbeans, PHPStorm, Aptana, Github, PDO
</ul>







<h2>Gary King Web Development</h2>

<ul>
<li>Designing and programming websites on a free lance basis.</li>
<li>Technology used: Expression Engine, Word Press, phpMyAdmin, notepad++, Adobe Creative Suite CS5, Microsoft Office, HTML5, XHTML, AJAX, Javascript, CSS, PHP, MYSQL, jQuery, Google API, Photoshop CS5, Dreamweaver CS5, Excel, Notepad++, Java, C++.</li> 


<li>I am currently finishing version 2 of a self designed Content Management System for recording studio owners, musicians and producers/recording engineers called discographyPro.  – Viewable at  discographyPro.com The site owner is able to build, maintain and update their discography  through the use of forms in an efficient dashboard format.  All content is submitted using jQuery, AJAX and PHP to store content in an SQL database</li>.  

</ul>

<h4>Older web site examples and experience  for reference </h4>

<ul>
<li>Draft FCB HTML Coder ( 2 month contract) Developed 'HTML email' packages for their Hewlett Packard account.</li>

<li>Developer for Purdie Rogers advertising agency: Sage Surfaces, Alta Logistics, LifeTime Lumber, Custom Bilt Metals, Mikron Vinyl, EnergyCore </li>
<li>Technology used: XHTML, AJAX, Javascript, CSS, PHP, MYSQL, jQuery, Google API, Photoshop CS3, Dreamweaver CS3,  Excel. </li>

<li>Designer and developer: Tera Vintage Estate Jewelry & Silver,  Mamas Mexican Restaurant (Seattle), Slippage (Seattle Rock Band), Accurate Heating and Electrical, discographyPro,</li>
<li>Technology used: XHTML, AJAX, Javascript, CSS, PHP, MYSQL, jQuery, Google API, Photoshop CS3, Dreamweaver CS3,  Excel. </li> 

<li>Some older sites: VideoTraining, Inc, I Want My Bailout Too, Video Training, Inc, Lava Lounge, Rebla Art and Hair Design, Helles Belles (Seattle AC/DC cover band), Baddbob (Artist portfolio), Stacey Greene, (Brooklyn Artist), Viable Video (Video Email), House of Leisure, I Drew My Music (Online music store), The Showbox (Seattle night club), Antakharana Yoga, Internet island Lounge (Kona), King County Choppers.
Technology used: XHTML, CSS, PHP, MYSQL, Photoshop CS3, Dreamweaver CS3, Excel.</li>

<li>Visit http://www.garykingwebdevelopment.com/site/resume/  for more info and examples. </li>
<li>Experienced with designing and coding from wireframes and other graphic mockups.</li>

</ul>





<h2>O t h e r   C o m p u t e r  / S o f t w a r e / O t h e r   E x p e r i e n c e</h2>

<ul> 
<li>Recent study and experience in the following languages: Java, C++ and creating a MVC framework in PHP.</li>
<li>Other technical experience includes 20 years as a recording engineer/producer and recording studio owner. SubPop, Soundgarden (A&M Records), Gruntruck (Roadrunner Records), Sony Records, CZ Records, Loosegroove, Suction, Kim Thayil and many others.</li>
<li>Professional audio programs: Pro Tools 10, Cubase. </li>
</ul>



<h2>O t h e r   W o r k    H i s t o r y</h2>

<h4>Gary King Web – 4/2007 – 2011</h4>
<ul>
<li>Free lance web developer</li>
</ul>

<h4>Video Training, Inc - 11/2000 – 04/2009. </h4> 
<ul>
<li>This company distributes training media. (The FISH! video, motivational sales, customer service training etc).</li>
<li>Office manager, bookkeeper and computer guru for this corporate training video distributor.</li>
<li>Design, maintaining and construction of their website</li>
<li>Technology used: CSS, PHP, MYSQL </li>
</ul>


<h4>Acura of Seattle - 11/98 - 3/99</h4>
<ul>
<li>Associate Finance Manager Worked for finance department finishing car deals by working with customers and lending institutions to complete auto loans and leases.</li>
<li>Managed contracts in transit (completed close to a million dollars in car deals).</li>
<li>Coordinated sales events.</li>
</ul> 

<h4>KOMO abc4 - 12/97 - 5/98 (Interim) </h4>
<ul>
<li>TV Sales Secretary Responsibilities included high intensity secretarial support for television sales department. </li>
<li>Prepared rate and avail packages, correspondence, proposals, sponsorships and Word and PowerPoint sales presentations and slide shows for general sales managers and staff (14 person sales staff).  All with a high degree of perfection and quality assurance.</li>
<li>Typing, editing and proofreading final text of letters, proposals and sales presentations.</li>
</ul>

<p>Gary King<br />
gary@garykingwebdevelopment.com<br />
www.garykingweb.com<br />
206-447-5196</p>

                
		</div>
                
        <h2>Testimonials (from website clients)</h2>
             
        <div class="pagePanelBorder">
            <ul>
            <li class="noBullet"><img src="../imgs/common/quotes.png" alt="testimonial" />&nbsp;&nbsp;Gary was great from the minute I called him. He was very  helpful and answered all my questions. He was always available when I needed him and our website turned out great! I definitely will use him again."</li>
            <li class="noBullet">... Kellie D., <a href="http://www.kandksuites.com" target="_blank" title="K and K Suites" >K and K Suites</a></li> 
            <li class="noBullet">&nbsp;</li>
            <li class="noBullet"><img src="../imgs/common/quotes.png" alt="testimonial" />&nbsp;&nbsp;In past, I've worked with offshore designers.  But I'm using Gary from now on. He provides way better service and true expertise.  Gary is GREAT!" </li>
            <li class="noBullet">... Stephen Nelson, CPA</li>
            </ul>
                
            <ul>
            <li class="noBullet">&nbsp;</li>
            <li class="noBullet"><img src="../imgs/common/quotes.png" alt="testimonial" />&nbsp;&nbsp;Gary created a beautiful website for me and I am completely happy with it!  His customer service is stellar and you can count on him to get the job done fast! You can be confident that you will be happy with the end product." </li>
            <li class="noBullet">...Terri K., <a href="http://www.teravintage.com" target="_blank" title="Tera Vintage Estate Jewelry and Silver" >Tera Vintage Estate Jewelry &amp; Silver</a></li>
            </ul>
        
        </div><!-- end pagePanelBorder -->
                
    </div><!-- end topSingCol -->
             
    <!-- content ends here -->
    
        </div><!-- end homePage -->
	</div><!-- end of page -->
    

</div><!-- end wrapper -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-9485654-3");
pageTracker._trackPageview();
} catch(err) {}</script>
<div class="bottomNav"><?php //include 'include/bottom-nav.php'; ?></div>
<div class="footer"><?php echo COPYRIGHT; ?></div>

</body>
</html>