    <link rel="stylesheet" type="text/css" href="contact/css/contact.css" />
	<!--<script type="text/javascript" src="contact/js/ajax.js" /></script>-->
    <script type="text/javascript" src="../public/js/jquery-validation/jquery.validate.min.js"/></script>

	<script type="text/javascript" src="../contact/js/contact.js" /></script>

    <?php include 'include/definitions.php'; ?>

    <h1>Contact</h1>	   
<div id="contactPrint"><!--start contactPrint ajax container -->
        <!-- start form content  -->

               
    <form id="contact_form" name="contact_form" method="post" action="">

    <table>
        <tr>
        <td colspan="3"><span class="warning">* Required</span>
        <p id="alert"></p>
        </td>
        </tr>
        
        <tr>
        <td class="leftTD"><span class="warning"> * </span>Name&nbsp;</td>
        <td colspan="2" class="centerTD"><label for="name"></label><input id="name" class="required contact" minlength="4"  size="35" name="name"  placeholder="Name" maxlength="50">	
        <p id="firstName" class="warning"></p>
        </tr>
        
        <tr>			
        <td class="leftTD"><span class="warning"> * </span>Email&nbsp;</span></td>
        <td colspan="2" class="centerTD"><label for="email"></label><input id="email" class="required email contact" size="35" name="email" type="email" placeholder="Email" maxlength="50" >
        <p id="emailAddress" class="warning"></p></td>  
        </tr>
              
        <tr>			
        <td class="leftTD">Subject&nbsp;</span></td>
        <td colspan="2" class="centerTD"><label for="subject"></label><input id="subject" class="contact" size="35" name="subject" maxlength="50" >
        <p id="emailAddress" class="warning"></p></td>  
        </tr>    
        

     
            <tr>
            <td class="leftTD"><!--<span class="warning"> * </span> -->Phone&nbsp;</td>
            <td class="centerTD"><label for="phone"></label><input id="phone" class="contact"  size="14" name="phone" maxlength="14" ><span id="telephone" class="warning"></span></td>
            <td class="rightTD">&nbsp;</td>
            </tr>
            
            <tr>
            <td class="leftTD">Fax&nbsp;</td>
            <td class="centerTD"><label for="fax"></label><input id="fax" class="contact" size="14" name="fax" maxlength="14"></td>
            <td class="rightTD">&nbsp;</td>
            </tr>
            
            <tr>
            <td class="leftTD">Website&nbsp;</td>
            <td class="centerTD"><label for="website"></label><input id="website" class="contact" size="35" name="website" maxlength="100" ></td>
            <td class="rightTD">&nbsp;</td>	
            </tr>
            
            <tr>
            <td class="leftTD">&nbsp;</td>
            <td class="centerTD">&nbsp;</td>
            <td class="rightTD">&nbsp;</td>	
            </tr>
            
            <tr>
            <td colspan="2" class="leftTD">&nbsp;</td>
            <td class="rightTD">&nbsp;</td>	
            </tr>
            
            <tr>
            <td class="leftTD">&nbsp;</td>
            <td colspan="2" class="centerTD"><label for="check1"></label><input id="check1" name="check1" type="checkbox" value="<?php echo CONTACT_OPTION_ONE; ?>" />
            <?php echo CONTACT_OPTION_ONE; ?></td>
           
            </tr>   
            
            <tr>
            <td class="leftTD">&nbsp;</td>
            <td colspan="2" class="centerTD"><label for="check2"></label><input id="check2" name="check2" type="checkbox" value="<?php echo CONTACT_OPTION_TWO; ?>" />
            <?php echo CONTACT_OPTION_TWO; ?>
            </td>
            
            </tr> 
            
            <tr>
            <td class="leftTD">&nbsp;</td>
            <td colspan="2" class="centerTD"><label for="check3"></label><input id="check3" name="check3" type="checkbox" value="<?php echo CONTACT_OPTION_THREE; ?>" />
            <?php echo CONTACT_OPTION_THREE; ?>
            </td>
           
            </tr>     
            
<!--            <tr>
            <td class="leftTD">&nbsp;</td>
            <td colspan="2" class="centerTD"><label for="check4"></label><input id="check4" name="check4" type="checkbox" value="<?php echo CONTACT_OPTION_FOUR; ?>" />
            <?php //echo CONTACT_OPTION_FOUR; ?>
            </td>-->
           
            </tr>     
    
            <tr>
            <td class="leftTD">&nbsp;</td>
            <td class="centerTD">&nbsp;</td>
            <td class="rightTD">&nbsp;</td>	
            </tr>     
            
            <tr>
            <td class="leftTD">&nbsp;</td>
            <td class="centerTD"><label for="refferal"></label><select id="referral" name="referral" class="contact">
            <option>How Did You Find Us?</option>
            <option>Advertisement</option>
            <option>Recommendation</option>
            <option>Online search</option>
            <option>Other</option>      
            </select>
            </td>
            <td class="rightTD">&nbsp;</td>	
            </tr>
            
            <tr>
            <td class="leftTD">&nbsp;</td>
            <td class="centerTD">&nbsp;</td>
            <td class="rightTD">&nbsp;</td>	
            </tr> 
            
            <tr>
            <td class="leftTD">&nbsp;</td>
            <td class="centerTD">Your message</td>
            <td class="rightTD">&nbsp;</td>	
            </tr> 
                
            <tr>
            <td class="leftTD">&nbsp;</td>
            <td class="centerTD"><label for="comments"></label><textarea id="comments" name="comments"></textarea></td>
            <td class="rightTD">&nbsp;</td>	
            </tr>   
            
            <tr>
            <td class="leftTD"><label for="contact_send_email"></label>
            <input id="contact_send_email" class="contact_send_email" type="submit" value="Send Email" /></td>
            <td class="centerTD">&nbsp;</td>
            <td class="rightTD">&nbsp;</td>	
            </tr>   
            </table>
                     
</form>
<!-- end of contact form -->
</div><!-- end contactPrint -->
