<!DOCTYPE html>
<?php 

require './classes/PageHead.php';
require './classes/PageData.php';
$meta = new pageHead( $metadata );
$meta->pagehead( $metadata );
$meta = new pageData( $pagedata );
$meta->pagedata( $pagedata ); 
$meta = new pageData( $employerdata );
$meta->employerdata( $employerdata ); 
$page_title = $metadata['page_title'] == 'Index' ?   'Home' :  $metadata['page_title']; 
//if( method_exists ( 'pageHead' , 'pagehead' )) { echo '<p>true</p>'; } else { echo '<p>false</p>'; }
//print_r($pagedata);
/*foreach ( $pagedata as  $skill => $skill_description ) {
 
		echo '<p>' . $skill .  $skill_description . '</p>';
		
}*/

/*echo $employerdata['employer1'];
echo $employerdata['employer2'];
echo $employerdata['employer3'];
*/
?>

    <!--[if lt IE 7]>
    <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
    <!--[if IE 7]>
    <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
    <!--[if IE 8]>
    <html class="no-js lt-ie9"> <![endif]-->
    <!--[if gt IE 8]><!-->
    <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <title><?php echo $page_title;  ?> | Web Developer | Seattle WA | Gary King Web Development </title>
				<?php //if( $metadata['page_title'] == 'Index' ) { echo 'Home'; } else { echo  $metadata['page_title']; } ?> 

        <!--[if lt IE 9]>
        <script type="text/javascript" src="js/ie-fixes.js"></script>
        <script type="text/javascript" src="js/_excanvas.compiled.js"></script>
        <link rel="stylesheet" href="css/ie-fixes.css">
        <![endif]-->

        <meta name="description" content="Gary King Web Developer">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
				<link rel="shortcut icon" href=" images/icons/favicon.png">
        <!--- This should placed first off all other scripts -->
 
             <link rel="stylesheet" href="css/font-awesome-ie7.css">
             <link rel="stylesheet" href="css/font-awesome-ie7.min.css">
             <link rel="stylesheet" href="css/font-awesome.css">
             <link rel="stylesheet" href="css/font-awesome.min.css">
             <link rel="stylesheet" href="css/revolution_settings.css">
             <link rel="stylesheet" href="css/bootstrap.min.css">
             <link rel="stylesheet" href="css/eislider.css">
             <link rel="stylesheet" href="css/tipsy.css">
             <link rel="stylesheet" href="css/prettyPhoto.css">
             <link rel="stylesheet" href="css/isotop_animation.css">
             <link rel="stylesheet" href="css/animate.css">
             <link rel="stylesheet" href="css/flexslider.css">
              
        <!-- remprod -->
        <!--<link rel="stylesheet" href="css/_colorpicker.css">-->
        <!-- end remprod -->

        <link href='css/style.css' rel='stylesheet' type='text/css'> 
        <link href='css/responsive.css' rel='stylesheet' type='text/css'>

        <link href="css/skins/blue.css" rel='stylesheet' type='text/css' id="skin-file">

        <!-- remprod -->
        <style type="text/css" id="skin-chooser-css">

        </style>
        <!-- end remprod -->

        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Vollkorn:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
        <script type="text/javascript" src="js/respond.js"></script>
<![endif]-->
        <!--<link rel="stylesheet" href="css/color-chooser.css">-->
        <link rel="stylesheet" href="css/overwrites.css">
    </head>