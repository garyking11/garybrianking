<footer>
    <div class="footer">

        <div class="container">
            <div class="footer-wrapper">
                <div class="row">


                    <!-- Footer Col. -->
                    <div class="col-md-3 col-sm-3 footer-col">
                        <div class="footer-content">
                            <div class="footer-content-logo">
                               <!-- <img src="images/logo.png" alt=""/>-->
                            </div>
                            <div class="footer-content-text">
                                <!--<p>Lorem ipsum dolor sit amet nec, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor.</p>
                                <p>Lorem ipsum dolor sit amet nec, consectetuer adipiscing elit. Aenean commodo ligula eget
                                dolor.</p>-->
                            </div>
                        </div>
                    </div>
                    <!-- //Footer Col.// -->

                    <!-- Footer Col. -->
                    <div class="col-md-3 col-sm-3 footer-col">

&nbsp;&nbsp;&nbsp;&nbsp;

                    </div>
                    <!-- //Footer Col.// -->

                    <!-- Footer Col. -->
                    <div class="col-md-3 col-sm-3 footer-col">
                        <div class="footer-title">

                        </div>
                        <div class="footer-content">

                            <span class="ft-schema" itemscope itemtype="http://schema.org/Person">
                                <span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">

                            <ul class="contact-info">
                                <li  class="social-icons">
                            		<a href="http://www.linkedin.com/in/garybking/" title="linkedin" target="_blank" class="social-media-icon linkedin-icon">linkedin</a>
                        			</li>
                                <li>
                                    <div class="contact-title">
                                        <i class="icon-phone"></i>phone
                                    </div>
                                    <div class="contact-details">

                                        <div itemprop="telephone">
                                            206-447-5196
                                        </div>

                                        <span itemprop="addressLocality">
                                        Seattle, </span>
                                        <span itemprop="addressRegion">WA</span>
                                        </div>

                                    </div>

                                    <div class="contact-title">
                                        <i class="icon-envelope"></i> eMail
                                    </div>

                                    <div class="contact-details">
                                    <a class="email" rel="gary|garykingweb.com?subject=Web Inquiry"
                                    title="Click here to email me">&nbsp;</a>
                                     </div>
                                </li>

                        </ul>
                                </span><!-- end Postal Address -->
                                </span><!-- end schema -->

                        </div>

                    <!-- //Footer Col.// -->

                    <!-- Footer Col. -->
                    <div class="col-md-3 col-sm-3 footer-col">
                        &nbsp;&nbsp;&nbsp;
                    </div>
                    <!-- //Footer Col.// -->

                        </div>
                    </div>
                </div>
            </div>

 </div>

<!-- start bottom -->


     <!--   <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="copyright-text">&copy; Gary B King </div>
                    </div>
                    <div class="col-md-6 col-sm-6">


                  &nbsp;&nbsp;&nbsp;
                  </div>
              </div>


            </div>
        </div>-->

</footer>

<!--</div> wrapper end -->


<!-- remprod -->
<script type="text/x-handlebars-template" id="css-skin">


    .navigation > li:hover > a,.navigation > li > a:hover,
    .navigation > li > .activelink,.navigation > li:hover > a > i, .navigation > li > a:hover >
    span.label-nav-sub::before, .navigation > li > a:focus > span.label-nav-sub::before{
    color:  {{skin_color}};
    }
    .mobile-menu-button, .mobile-nav, .slider-fixed-container{
    background-color:  {{skin_color}};
    }

    .navigation > li > a:after, .navigation>li>.activelink:after{
    background-color:  {{skin_color}};
    }
    .navigation>li:hover > a > span.label-nav-sub::before,.navigation>li:hover> a > span.label-nav-sub::before{
    color:  {{skin_color}};
    }


    /* Page Content Colors */
    .body-wrapper a:hover , .tab a:hover, accordion .title:hover, .top-body a:hover, .bottom-body
    a:hover,.content-box.content-style2 h4 i
    ,.accordion .active h4, .accordion .title:hover h4, .side-navigation .menu-item.current-menu-item a,
    .side-navigation .menu-item:hover a:after,
    .side-navigation .menu-item:hover a, a.tool-tip, .team-member .team-member-position, .team-member-progress
    .team-member-position, .item-img-overlay i
    ,ul.icon-content-list-container li.icon-content-single .icon-box i,.item-img-overlay .portfolio-zoom:hover,
    .navigation ul li:hover>a, .blog_post_quote:after,
    .item-img-overlay .portfolio-zoom, body .white-text .feature-details a:hover{
    color:  {{skin_color}};
    }
    ::selection
    {
    background-color: {{skin_color}};
    }
    ::-moz-selection
    {
    background-color: {{skin_color}};
    }
    .item-img-overlay .portfolio-zoom:hover, .tab a.active {
    color:  {{skin_color}} !important;
    }
    .callout-box{
    border-left-color:  {{skin_color}}
    }
    .feature .feature-content,.team-member .team-member-content{
    border-top-color:  {{skin_color}};
    }

    .team-member-progress-bar{
    background-color:  {{skin_color}};
    }
    .blog-masonry .blog_post_quote{
    border-top: 2px solid  {{skin_color}};
    }
    .tab a.active:after {
    background-color: {{skin_color}};
    border-color:  {{skin_color}};
    }
    .item-img-overlay{
    background-color:  {{skin_color_rgba}};
    }
    .item-img-overlay i:hover{
    background-color:  {{skin_color_rgba}};
    }
    body .section-content.section-image{
    background-color:  {{skin_color_rgba}};
    }



    .button, .body-wrapper input[type="submit"], .body-wrapper input[type="button"], .section-content.section-color-bg,.content-box.content-style4 h4 i
    ,button.button-main,.body-wrapper .tags a:hover,.callout-box.callout-box2, .blog-search .blog-search-button, .top-title-wrapper, .testimonial-big
    ,.content-style3:hover .content-style3-icon, .content-box.style5 h4 i, table.table thead tr, .price-table .price-label-badge, .price-table .price-table-header, .section-subscribe .subscribe-button.icon-envelope-alt{
    background-color: {{skin_color}};
    }


    .blog-post-icon,.comments-list .children .comment:before,.portfolio-filter li a.portfolio-selected,
    .portfolio-filter li a:hover, .dropcaps.dropcaps-color-style, .carousel-container .carousel-icon:hover{
    background-color:  {{skin_color}};
    }
    .comments-list .children .comment:after{
    border-color: transparent transparent transparent  {{skin_color}};;
    }

    .highlighted-text{
    background-color:  {{skin_color}};
    color: #ffffff;
    }
    .icons-list.colored-list li:before, .blog-post-date .day, .blog-post-date .month, strong.colored, span.colored
    ,.content-style3 .content-style3-icon{
    color:  {{skin_color}};
    }


    .pagination .prev, .pagination .next, .pagination a:hover, .pagination a.current, .price-table .price-label{
    color: {{skin_color}};
    }


    /* Footer Area Colors */

    .footer .copyright{
    border-color:  {{skin_color}};
    }

    .footer .copyright a:hover{
    color:  {{skin_color}};
    }
    .flickr_badge_wrapper .flickr_badge_image img:hover{
    border-color:  {{skin_color}};
    }
</script>


<script type="text/javascript"> var $default_skin = "light-blue";</script><div id="skin-chooser-container" class="skin-chooser-container hide-skin-chooser">
    <a href="" target="_blank" class="skin-save" id="skin-save"><i class="icon-arrow-down"></i> DOWNLOAD NEW SKIN </a>
    <div class="skin-chooser">

        <div class="skin-chooser-label">
            Layout mode
        </div>
        <div class="skin-chooser-row skin-chooser-row-open">
            <select id="layout-mode">
                <option value="">Wide</option>
                <option value="boxed">Boxed</option>
            </select>
        </div>
        <div class="skin-chooser-label">
            Predefined Skins
        </div>
        <div class="skin-chooser-row skin-chooser-row-open">
                            <div title="light-blue" class="predefined-skins light-blue" data-skinname="light-blue" style="background-color: #279FBB"></div>
                            <div title="green" class="predefined-skins green" data-skinname="green" style="background-color: #26ae91"></div>
                            <div title="red" class="predefined-skins red" data-skinname="red" style="background-color: #d14836"></div>
                            <div title="pink" class="predefined-skins pink" data-skinname="pink" style="background-color: #bb3b6b"></div>
                            <div title="light-purple" class="predefined-skins light-purple" data-skinname="light-purple" style="background-color: #bba6bb"></div>
                            <div title="orange" class="predefined-skins orange" data-skinname="orange" style="background-color: #dd6153"></div>
                            <div title="bright-green" class="predefined-skins bright-green" data-skinname="bright-green" style="background-color: #8cbb75"></div>
                            <div title="dark-blue" class="predefined-skins dark-blue" data-skinname="dark-blue" style="background-color: #2580b1"></div>
                            <div title="oil" class="predefined-skins oil" data-skinname="oil" style="background-color: #9b9e40"></div>
                            <div title="black" class="predefined-skins black" data-skinname="black" style="background-color: #5f5d5c"></div>
                            <div title="light-brown" class="predefined-skins light-brown" data-skinname="light-brown" style="background-color: #d06f71"></div>
                            <div title="coffee" class="predefined-skins coffee" data-skinname="coffee" style="background-color: #9d634c"></div>
                            <div title="flat-blue" class="predefined-skins flat-blue" data-skinname="flat-blue" style="background-color: #3498db"></div>
                    </div>

        <div class="skin-chooser-label">
            Custom Colors
        </div>

        <div class="skin-chooser-row skin-chooser-row-open">
            <div class="skin-chooser-elements">
                                    <div class="color-chooser-group-label">
                        <i class="icon-angle-right"></i> Choose your skin color                    </div>
                                            <div class="color-chooser-input">
                            <input type="text" name="skin_color" id="skin_color" class="input-colorpicker"
                                   value="#279FBB"/>
                            <label class="color-preview" for="skin_color"
                                   style="background-color: #279FBB"></label>
                        </div>
                                </div>
        </div>
        <div class="skin-chooser-label">
            Patterns Background
        </div>

        <div class="skin-chooser-row skin-chooser-row-open">
                            <div class="pattern pattern-black-twill" data-body-class="bgpattern-black-twill"></div>
                            <div class="pattern pattern-dark-fish-skin" data-body-class="bgpattern-dark-fish-skin"></div>
                            <div class="pattern pattern-escheresque-ste" data-body-class="bgpattern-escheresque-ste"></div>
                            <div class="pattern pattern-grey" data-body-class="bgpattern-grey"></div>
                            <div class="pattern pattern-knitting250px" data-body-class="bgpattern-knitting250px"></div>
                            <div class="pattern pattern-p4" data-body-class="bgpattern-p4"></div>
                            <div class="pattern pattern-p5" data-body-class="bgpattern-p5"></div>
                            <div class="pattern pattern-p6" data-body-class="bgpattern-p6"></div>
                            <div class="pattern pattern-ps-neutral" data-body-class="bgpattern-ps-neutral"></div>
                            <div class="pattern pattern-pw-maze-white" data-body-class="bgpattern-pw-maze-white"></div>
                            <div class="pattern pattern-pw-pattern" data-body-class="bgpattern-pw-pattern"></div>
                            <div class="pattern pattern-retina-wood" data-body-class="bgpattern-retina-wood"></div>
                            <div class="pattern pattern-shattered" data-body-class="bgpattern-shattered"></div>
                            <div class="pattern pattern-subtle-dots" data-body-class="bgpattern-subtle-dots"></div>
                            <div class="pattern pattern-subtle-surface" data-body-class="bgpattern-subtle-surface"></div>
                            <div class="pattern pattern-whitediamond" data-body-class="bgpattern-whitediamond"></div>
                    </div>

        <div class="skin-chooser-label">
            Images Background
        </div>
        <div class="skin-chooser-row skin-chooser-row-open">
                        <div class="image-chooser tuscany-thumb" data-body-class="bg-tuscany"></div>
                        <div class="image-chooser tracks-thumb" data-body-class="bg-tracks"></div>
                        <div class="image-chooser blurred-lines-thumb" data-body-class="bg-blurred-lines"></div>
                        <div class="image-chooser mountain-thumb" data-body-class="bg-mountain"></div>
                    </div>



    </div>
    <!--<a href="#" class="arrow-left  icon-cogs" id="show_hide_skin_chooser hide"></a>-->

</div>
<!-- end remprod -->
<script type="text/javascript" src="../js/_jq.js"></script>

<script type="text/javascript" src="../js/_jquery.placeholder.js"></script>

<!-- remprod -->
<!--<script type="text/javascript" src="../js/_colorpicker.js"></script>
<script type="text/javascript" src="../js/_handlebars.js"></script>
<script type="text/javascript" src="../js/_skins.js"></script>
<script type="text/javascript" src="../js/_skinchooser.js"></script>-->
<!-- end remprod -->




<script src="../js/activeaxon_menu.js" type="text/javascript"></script>
<script src="../js/animationEnigne.js" type="text/javascript"></script>
<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/easypiecharts.js" type="text/javascript"></script>
<script src="../js/ie-fixes.js" type="text/javascript"></script>
<script src="../js/jquery.base64.js" type="text/javascript"></script>
<script src="../js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript"></script>
<script src="http://malsup.github.com/jquery.cycle2.js"></script>

<script src="../js/jquery.cycle.js" type="text/javascript"></script>
<script src="../js/jquery.cycle2.carousel.js" type="text/javascript"></script>
<script src="../js/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="../js/jquery.easytabs.js" type="text/javascript"></script>
<script src="../js/jquery.eislideshow.js" type="text/javascript"></script>
<script src="../js/jquery.flexslider.js" type="text/javascript"></script>
<script src="../js/jquery.infinitescroll.js" type="text/javascript"></script>
<script src="../js/jquery.isotope.js" type="text/javascript"></script>
<script src="../js/jquery.parallax-1.1.3.js" type="text/javascript"></script>
<script src="../js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="../js/jQuery.scrollPoint.js" type="text/javascript"></script>
<script src="../js/jquery.themepunch.plugins.min.js" type="text/javascript"></script>
<script src="../js/jquery.themepunch.revolution.js" type="text/javascript"></script>
<script src="../js/jquery.tipsy.js" type="text/javascript"></script>
<script src="../js/jquery.validate.js" type="text/javascript"></script>
<script src="../js/jQuery.XDomainRequest.js" type="text/javascript"></script>
<!--<script src="../js/retina.js" type="text/javascript"></script> -->
<script src="../js/timeago.js" type="text/javascript"></script>
<script src="../js/tweetable.jquery.js" type="text/javascript"></script>
<script src="../contact/js/contact.js" type="text/javascript"></script>
<script src="../js/jquery.emailProt.js" type="text/javascript"></script>
<script src="../js/zeina.js" type="text/javascript"></script>
<script>
$(document).ready(function() {



/* obfuscate email */
 var $emailLinks = $('a.email');

	if ($emailLinks.length < 1) {return;} // skip this function if no objects found
	//alert($('a.email')).attr('href');
		// Insert empty <a> tag with the following attributes:
		// * class="email"
		// * rel="example|domain.com" where pipe char '|' replaces '@'
		// * title="Email Us", this is the text shown after the email link is created by js
		$emailLinks.addClass('addicon').emailProt();
});
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43688158-1', 'auto');
  ga('send', 'pageview');

</script>

</body>
</html>
