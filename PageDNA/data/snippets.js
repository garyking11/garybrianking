[
    {
        "sequence": ["blue", "yellow", "orange", "red"],
        "states": {
            "red": "A",
            "orange": "A",
            "yellow": "A",
            "blue": "A"
        }
    },
    {
        "sequence": ["orange", "yellow", "red", "blue"],
        "states": {
            "red": "B",
            "orange": "B",
            "yellow": "A",
            "blue": "A"
        }
    },
    {
        "sequence": ["blue", "yellow", "red", "orange"],
        "states": {
            "red": "A",
            "orange": "A",
            "yellow": "B",
            "blue": "B"
        }
    },
    {
        "sequence": ["yellow", "blue", "orange", "red"],
        "states": {
            "red": "A",
            "orange": "B",
            "yellow": "A",
            "blue": "B"
        }
    }
]


/////



    [
    {
        "sequence": ["red", "green", "yellow", "blue"],
        "states": {
            "red": "A",
            "green": "A",
            "yellow": "A",
            "blue": "A"
        }
    },
    {
        "sequence": ["green", "yellow", "red", "blue"],
        "states": {
            "green": "B",
            "yellow": "A",
            "red": "B",
            "blue": "A"
        }
    },
    {
        "sequence": ["blue", "yellow", "red", "green"],
        "states": {
            "blue": "B",
            "yellow": "B",
            "red": "A",
            "green": "A"
        }
    },
    {
        "sequence": ["yellow", "blue", "green", "red"],
        "states": {
            "yellow": "A",
            "blue": "B",
            "green": "B",
            "red": "A"



        }
    }
    ]




    //////////



<!doctype html>
<html class="no-js" ng-app='app' lang="">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>PageDNA</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->
<script src="public/js/modernizr-2.8.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="public/css/bootstrap-3.3.6-dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="public/css/local.css">

    <script src="public/components/angularjs/angular.min.js"></script>
    <script src="public/app.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->



    </head>
    <body>



    <div class="container" data-ng-controller="TestCtrl">

    <div class=".col-md-12">

    <h1>PageDNA</h1>
    <h2>{{test}}</h2>


<table>

<tr><th>A</th><th>B</th></tr>

<tr data-ng-repeat="row in data">

    <td>
    <span data-ng-repeat="(color, box) in row['states']" ng-click="setState($index)">

    <span class="color-box"
ng-class="color"
ng-show="box == 'A'"
ng-click="setState()">

    {{color}}
</span>
</span>
</td>

<td>
<span data-ng-repeat="(color, box) in row['states']" ng-click="setState()">

    <span class="color-box"
ng-class="color"
ng-show="box == 'B'">

    {{color}}
</span>

</span>
</td>

</tr>

</table>



</div>


</div>

<div class="container">

    <div class=".col-md-12">
    <h5>NOTES:</h5>
<p></p>
</div>
</div>

<script src="public/components/angularjs/angular-route.min.js"></script>
    <script src="public/controllers/test.controller.js"></script>

    </body>
    </html>
